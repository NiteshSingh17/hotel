"use client";

import {
  collection,
  documentId,
  getDocs,
  onSnapshot,
  query,
  where,
} from "firebase/firestore";
import { HotelInfoDetails } from "../classes/HotelsInfoDetails";
import { HOTEL_COLLECTION_NAME } from "../constant";
import { db } from "./config";

export async function getHotelSnapshot(
  cb: (data: HotelInfoDetails) => void,
  id: string
): Promise<(() => void) | undefined> {
  if (typeof cb !== "function") {
    console.log("Error: The callback parameter is not a function");
    return;
  }

  let q = query(
    collection(db, HOTEL_COLLECTION_NAME),
    where(documentId(), "==", id)
  );

  const unsubscribe = onSnapshot(q, (querySnapshot) => {
    const results = querySnapshot.docs.map((doc) => {
      return {
        id: doc.id,
        ...doc.data(),
      };
    });
    cb(results[0] as unknown as HotelInfoDetails);
  });

  return unsubscribe;
}

export async function getHotels(): Promise<HotelInfoDetails[]> {
  console.log("getHotels fun");
  const collectionData = collection(db, HOTEL_COLLECTION_NAME);
  let hotelsData = await getDocs(collectionData).then((todo) => {
    let hotels = todo.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    return hotels;
  });
  return hotelsData as unknown as HotelInfoDetails[];
}
