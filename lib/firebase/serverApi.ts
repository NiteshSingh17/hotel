"use server";
import { addDoc, collection, doc, updateDoc } from "firebase/firestore";
import {
  deleteObject,
  getDownloadURL,
  getStorage,
  ref,
  uploadBytes,
} from "firebase/storage";
import { HotelInfoDetails, ImagesList } from "../classes/HotelsInfoDetails";
import { HOTEL_COLLECTION_NAME } from "../constant";
import { db } from "./config";

export async function createHotel(
  data: Omit<HotelInfoDetails, "id">
): Promise<HotelInfoDetails["id"]> {
  const collectionRef = collection(db, HOTEL_COLLECTION_NAME);
  let hotel = await addDoc(collectionRef, data);
  return hotel.id;
}

export async function uploadStorage(formData: FormData): Promise<ImagesList> {
  const storage = getStorage();
  const file = formData.get("file") as File;
  const storageRef = ref(storage, file.name);
  const uploadedFile = await uploadBytes(storageRef, file);
  const uploadedFileUrl = await getDownloadURL(uploadedFile.ref);
  return {
    imageUrl: uploadedFileUrl,
    imageId: uploadedFile.metadata.md5Hash ?? "",
    imageTitle: uploadedFile.metadata.name,
  };
}

export async function deletetorage(fileName: string) {
  const storage = getStorage();
  const storageRef = ref(storage, fileName);
  await deleteObject(storageRef);
}

export async function updateHotel(
  id: HotelInfoDetails["id"],
  data: Partial<Omit<HotelInfoDetails, "id">>
) {
  await updateDoc(doc(db, HOTEL_COLLECTION_NAME, id), data);
}
