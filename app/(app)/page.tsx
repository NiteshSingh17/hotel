"use client";

// we have to use use client here because on deployment if we add new hotel and move to home page it was still not showing new hotel  because of cache of page.
import { HotelInfoDetails } from "@/lib/classes/HotelsInfoDetails";
import { getHotels } from "@/lib/firebase/clientApi";
import Link from "next/link";
import { useEffect, useState } from "react";
import { SuccessButton } from "../_components/Buttons";

export default function Home() {
  const [hotels, setHotels] = useState<HotelInfoDetails[]>([]);

  useEffect(() => {
    getHotels().then(setHotels);
  }, []);

  return (
    <main className="min-h-screen p-24">
      <Link href="/hotel/">
        <SuccessButton>Create New Hotel</SuccessButton>
      </Link>
      <div className="mt-20 space-y-10">
        {hotels.map((hotel) => (
          <div key={hotel.id}>
            <Link href={`/hotel/${hotel.id}`}>
              <div className="font-bold text-2xl">{hotel.hotelName}</div>
              <div className="text-sm text-gray-400 mt-2">
                Email : {hotel.hotelEmailId === "" ? "--" : hotel.hotelEmailId}
              </div>
            </Link>
          </div>
        ))}
      </div>
    </main>
  );
}
