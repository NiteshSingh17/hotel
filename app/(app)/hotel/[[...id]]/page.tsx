import { HotelDetails } from "@/app/_components/HotelDetails";

export default function Page({ params }: { params: { id?: string[] } }) {
  const hotelId = params.id?.[0];
  return <HotelDetails hotelId={hotelId} />;
}
