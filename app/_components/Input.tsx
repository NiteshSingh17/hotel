"use client";

import {
  ChangeEventHandler,
  FocusEventHandler,
  MouseEventHandler,
  useEffect,
  useState,
} from "react";
import { DangerButton, SuccessButton } from "./Buttons";

interface InputProps {
  label: string;
  onUpdate: (value: string | number) => void;
  value: string | number;
  type?: HTMLInputElement["type"];
}

export function Input({
  label,
  value,
  onUpdate,
  type,
}: InputProps): JSX.Element {
  const [data, setData] = useState<InputProps["value"]>(value);
  const [showActions, setShowActions] = useState(false);

  const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    setData(event.target.value);
  };

  useEffect(() => {
    setData(value);
    setShowActions(false);
  }, [value]);

  const handleBlur: FocusEventHandler<HTMLDivElement> = () => {
    setData(value);
  };

  const handleCancel: MouseEventHandler<HTMLButtonElement> = () => {
    setData(value);
    setShowActions(false);
  };

  const handleFocus: FocusEventHandler<HTMLInputElement> = () => {
    setShowActions(true);
  };

  const handleSave: MouseEventHandler<HTMLButtonElement> = () => {
    onUpdate(data);
  };

  return (
    <div>
      <div>
        <label>{label}</label>
      </div>
      <input
        onFocus={handleFocus}
        className="mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        value={data}
        onChange={handleChange}
        type={type}
      />
      {showActions === true ? (
        <div className="mt-6 flex gap-md">
          <SuccessButton onClick={handleSave}>Save</SuccessButton>
          <DangerButton onClick={handleCancel}>Cancel</DangerButton>
        </div>
      ) : null}
    </div>
  );
}

interface CheckboxProps {
  label: string;
  onUpdate: (value: boolean) => void;
  isChecked: boolean;
}
export function CheckBox({
  label,
  onUpdate,
  isChecked,
}: CheckboxProps): JSX.Element {
  const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    onUpdate(event.target.checked);
  };
  return (
    <div className="flex items-center">
      <input
        checked={isChecked}
        type="checkbox"
        onChange={handleChange}
        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
      />
      <label className="ms-2 text-sm font-medium text-gray-400 dark:text-gray-500">
        {label}
      </label>
    </div>
  );
}
