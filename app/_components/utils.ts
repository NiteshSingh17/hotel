import { HotelInfoDetails } from "@/lib/classes/HotelsInfoDetails";

export const hotelDefaultValue: Omit<HotelInfoDetails, "id"> = {
  hotelName: "",
  hotelEmailId: "",
  hotelContactNumber: 0,
  hotelLandmark: "",
  hotelAddress: "",
  hotelStartingPrice: 0,
  hotelDescription: "",
  hotelStarRating: 0,
  hotelImageUrl: "",
  hotelState: "",
  hotelCity: "",
  hotelCountry: "",
  hotelRegion: "",
  hotelPincode: "",
  hotelSlugsDetails: {
    hotel: "",
    hotelCity: "",
    hotelRegion: "",
    hotelState: "",
    hotelCountry: "",
  },
  hotelLongitude: 0,
  hotelLatitude: 0,
  hotelMapUrl: "",
  hotelPaymentOption: {
    postpaidPayment: true,
    prepaidPayment: true,
    partialPayment: true,
  },
  hotelImagesList: [],
};
