"use client";

import { ImageList, ImageListProps } from "@/app/_components/ImageList";
import { CheckBox, Input } from "@/app/_components/Input";
import { HotelInfoDetails, ImagesList } from "@/lib/classes/HotelsInfoDetails";
import { getHotelSnapshot } from "@/lib/firebase/clientApi";
import { createHotel, updateHotel } from "@/lib/firebase/serverApi";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { hotelDefaultValue } from "./utils";

export function HotelDetails({ hotelId }: { hotelId?: string }) {
  const [hotelData, setHotelData] = useState<HotelInfoDetails>();
  const router = useRouter();
  useEffect(() => {
    if (hotelId !== undefined) {
      let unsubscribe: (() => void) | undefined;
      (async () => {
        unsubscribe = await getHotelSnapshot((data) => {
          setHotelData(data);
        }, hotelId);
      })();
      return () => {
        unsubscribe?.();
      };
    }
  }, [hotelId]);

  const handleUpdate = async (
    name: string,
    value: string | number | boolean | ImagesList[]
  ) => {
    if (hotelId === undefined) {
      const newHotelId = await createHotel({
        ...hotelDefaultValue,
        [name]: value,
      });
      router.push(`/hotel/${newHotelId}`);
    } else if (typeof hotelData?.id === "string") {
      updateHotel(hotelData?.id, { [name]: value });
    }
  };

  const handlePaymentUpdate = async (name: string, value: boolean) => {
    if (hotelId === undefined) {
      const newHotelId = await createHotel({
        ...hotelDefaultValue,
        hotelPaymentOption: {
          ...hotelDefaultValue.hotelPaymentOption,
          [name]: value,
        },
      });
      router.push(`/hotel/${newHotelId}`);
    } else if (typeof hotelData?.id === "string") {
      updateHotel(hotelData?.id, {
        hotelPaymentOption: {
          ...hotelData.hotelPaymentOption,
          [name]: value,
        },
      });
    }
  };

  const getUpddateHanler = (name: string) => ({
    onUpdate: (value: string | number | boolean) => handleUpdate(name, value),
  });

  const handleImageUpload: ImageListProps["onAddImage"] = (image) => {
    const newImages = [...(hotelData?.hotelImagesList ?? []), image];
    handleUpdate("hotelImagesList", newImages);
  };

  const handleImageDelete: ImageListProps["onRemove"] = (image) => {
    const newImages = hotelData?.hotelImagesList.filter(
      (img) => img.imageId !== image.imageId
    );
    handleUpdate("hotelImagesList", newImages ?? []);
  };

  return (
    <div className="min-h-screen p-24">
      <div className="mb-10">
        <ImageList
          images={hotelData?.hotelImagesList ?? []}
          onAddImage={handleImageUpload}
          onRemove={handleImageDelete}
        />
      </div>
      <main className="grid grid-cols-2	gap-4">
        <Input
          label="Hotel Name"
          value={hotelData?.hotelName ?? ""}
          {...getUpddateHanler("hotelName")}
        />
        <Input
          label="Email"
          type="email"
          value={hotelData?.hotelEmailId ?? ""}
          {...getUpddateHanler("hotelEmailId")}
        />
        <Input
          label="Contact"
          type="number"
          value={hotelData?.hotelContactNumber ?? ""}
          {...getUpddateHanler("hotelContactNumber")}
        />
        <Input
          label="Landmark"
          value={hotelData?.hotelLandmark ?? ""}
          {...getUpddateHanler("hotelLandmark")}
        />
        <Input
          label="Address"
          value={hotelData?.hotelAddress ?? ""}
          {...getUpddateHanler("hotelAddress")}
        />
        <Input
          label="Starting price"
          type="number"
          value={hotelData?.hotelStarRating ?? ""}
          {...getUpddateHanler("hotelStartingPrice")}
        />
        <Input
          label="Stars"
          value={hotelData?.hotelStarRating ?? ""}
          type="number"
          {...getUpddateHanler("hotelStarRating")}
        />
        <Input
          label="State"
          value={hotelData?.hotelState ?? ""}
          {...getUpddateHanler("hotelState")}
        />
        <Input
          label="City"
          value={hotelData?.hotelCity ?? ""}
          {...getUpddateHanler("hotelCity")}
        />
        <Input
          label="Country"
          value={hotelData?.hotelCountry ?? ""}
          {...getUpddateHanler("hotelCountry")}
        />
        <Input
          label="Region"
          value={hotelData?.hotelRegion ?? ""}
          {...getUpddateHanler("hotelRegion")}
        />
        <Input
          label="Pincode"
          value={hotelData?.hotelPincode ?? ""}
          type="number"
          {...getUpddateHanler("hotelPincode")}
        />

        <Input
          label="Longtitude"
          type="number"
          value={hotelData?.hotelLongitude ?? ""}
          {...getUpddateHanler("hotelLongitude")}
        />
        <Input
          label="Latitude"
          type="number"
          value={hotelData?.hotelLatitude ?? ""}
          {...getUpddateHanler("hotelLatitude")}
        />
        <Input
          label="hotelMapUrl"
          value={hotelData?.hotelMapUrl ?? ""}
          {...getUpddateHanler("hotelMapUrl")}
        />
        <Input
          label="Pincode"
          type="number"
          value={hotelData?.hotelPincode ?? ""}
          {...getUpddateHanler("hotelPincode")}
        />
        <CheckBox
          label="Post paid payment"
          isChecked={hotelData?.hotelPaymentOption?.postpaidPayment ?? false}
          onUpdate={(isChecked) =>
            handlePaymentUpdate("postpaidPayment", isChecked)
          }
        />
        <CheckBox
          label="Pre paid payment"
          isChecked={hotelData?.hotelPaymentOption?.prepaidPayment ?? false}
          onUpdate={(isChecked) =>
            handlePaymentUpdate("prepaidPayment", isChecked)
          }
        />
        <CheckBox
          label="Partial paid payment"
          isChecked={hotelData?.hotelPaymentOption?.partialPayment ?? false}
          onUpdate={(isChecked) =>
            handlePaymentUpdate("partialPayment", isChecked)
          }
        />
      </main>
    </div>
  );
}
