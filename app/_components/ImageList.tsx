import { ImagesList } from "@/lib/classes/HotelsInfoDetails";
import { deletetorage, uploadStorage } from "@/lib/firebase/serverApi";
import Image from "next/image";
import { ChangeEventHandler, MouseEventHandler, useRef, useState } from "react";
import { DangerButton } from "./Buttons";

export interface ImageListProps {
  images: ImagesList[];
  onAddImage: (images: ImagesList) => void;
  onRemove: (images: ImagesList) => void;
}

export function ImageList({
  onAddImage,
  images,
  onRemove,
}: ImageListProps): JSX.Element {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [isUploadinImage, setIsUploadingImage] = useState(false);
  const [isDeletingImage, setIsDeletingImage] = useState(false);

  const handlAddImage: MouseEventHandler<HTMLDivElement> = () => {
    if (isUploadinImage === false) inputRef.current?.click();
  };

  const handleFileChange: ChangeEventHandler<HTMLInputElement> = async (
    event
  ) => {
    const files = event.target.files;
    if (files !== null && files.length > 0) {
      const uploadFile = files[0];
      setIsUploadingImage(true);
      try {
        const formData = new FormData();
        formData.append("file", uploadFile);
        let fileData = await uploadStorage(formData);
        onAddImage?.(fileData);
      } catch (error) {
      } finally {
        setIsUploadingImage(false);
      }
    }
  };

  const deleteImage = async (file: ImagesList) => {
    if (isDeletingImage === true) return;
    setIsDeletingImage(true);
    try {
      await deletetorage(file.imageTitle);
      onRemove(file);
    } catch (error) {
    } finally {
      setIsDeletingImage(false);
    }
  };

  return (
    <div className="flex flex-wrap h-60 gap-4">
      {images.map((image) => (
        <div className="flex flex-col w-1/4 max-w-1/4" key={image.imageUrl}>
          <div className="flex-1 relative">
            <Image fill={true} src={image.imageUrl} alt={image.imageTitle} />
          </div>
          <div className="mt-4">
            <DangerButton
              className={isDeletingImage ? "cursor-not-allowed" : undefined}
              onClick={() => deleteImage(image)}
            >
              Remove
            </DangerButton>
          </div>
        </div>
      ))}
      <div className="w-1/4">
        <div
          onClick={handlAddImage}
          className="flex justify-center items-center bg-gray-700 w-full h-full rounded cursor-pointer"
        >
          {isUploadinImage === true ? "Uploading..." : "Add Image"}
        </div>
      </div>
      <input
        ref={inputRef}
        onChange={handleFileChange}
        className="hidden"
        type="file"
        multiple={false}
        accept="image/png, image/gif, image/jpeg"
      />
    </div>
  );
}
